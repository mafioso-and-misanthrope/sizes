import gensim.downloader as api 


def get_dataset():
    return api.load("wiki-english-20171001")


def download():
    print("Getting model...")
    path_model = api.load("wiki-english-20171001", return_path=True)
    print("Model successfully saved in " + path_model)

