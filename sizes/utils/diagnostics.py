#!/usr/bin/env python 


from sizes.configurator import *


if settings["EnableDiagnostics"]:
    from time import time


def measure_time(comment, foo, *args):
    if settings["EnableDiagnostics"]:
        print(comment)
        start_time = time()
        result = foo(*args)
        print("Elapsed time: " + str(time() - start_time) + " s")
        return result
    else:
        return foo(*args)

