import os

from collections import Counter

from sizes.configurator import *
from sizes.utils.diagnostics import measure_time


def clear_pickle_cache():
    for file in os.listdir(settings["PickleDir"]):
        os.remove(os.path.join(settings["PickleDir"], file))


def on_start():
    if not os.path.exists(settings["TextDir"]):
        os.makedirs(settings["TextDir"])
    if not os.path.exists(settings["ModelDir"]):
        os.makedirs(settings["ModelDir"])
    if not os.path.exists(settings["PickleDir"]):
        os.makedirs(settings["PickleDir"])
    if not os.path.exists(settings["ResultDir"]):
        os.makedirs(settings["ResultDir"])

    if settings["ClearPickleCache"]:
        clear_pickle_cache()

    print("Launched with parameters:")
    print("* separate download of Wikipedia:\t\t" + str(settings["SeparatedDownload"]))
    print("* allow random order reading wiki articles:\t" + str(settings["UseRandom"]))
    print("* allow diagnostic messages and measurement:\t" + str(settings["EnableDiagnostics"]))
    print()

def create_vocabulary(dataset):
    vocab = set()
    for pair in dataset.X:
        vocab.add(pair[0])
        vocab.add(pair[1])
    return vocab

