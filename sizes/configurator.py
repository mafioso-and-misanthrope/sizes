#!/usr/bin/env python

settings = {

    # Main
    "WordsCount"        : 1000000 ,

    ### 
        # Dataset downloading is provided by gensim.downloader.load and it's saving in ~/gensim-data.
        # Downloader supports downloading and creating dataset object at the same time.
        # In this case Linux users may be confronted with tmpfs-partition overflow
        # (it is located in RAM and occupies half of RAM capacity) although dataset downloads
        # in ~/gensim-data ultimately.

        # If you have enogh disk space (~6.3 Gb) but get a memory overflow exception downloading the dataset,
        # set SEPARATED_DOWNLOAD = True. In this case dataset will not be downloaded in tmpfs first.
 
        # Скачивание датасета происходит стандартными средствами gensim - функцией gensim.downloader.load,
        # которая поддерживает закачку и формирование объекта датасета "на лету". При полном скачивании
        # датасета (~6,3 Гб) может возникнуть переполнение раздела tmpfs, который по умолчанию размещается
        # в ОЗУ и имеет максимальный размер в половину от всей доступной памяти.

        # Если у вас малый суммарный объём ОЗУ и раздела swap, рекомендуется скачать данные предварительно.
        # Для этого установите флаг SEPARATED_DOWLOAD = True
    ###

    "SeparatedDownload"     : False ,
    "UseRandom"             : False ,    # Allow random order reading wiki articles (sometimes can increase the results).
                                         # WARNING! Has no effect if ALLOW_READ_PICKLE_CACHE is True or ALLOW_READ_MODEL is True

    # Cache
    "ClearPickleCache"      : False ,
    "AllowReadPickleCache"  : True  ,    # Enable caching of preprocessed Wikipedia sentences
    "AllowWritePickleCache" : True  ,

    "AllowReadModel"        : True  ,    # Allow to use model saved before
                                         # WARNING! If True, WE training parameters have no effect!


    # Path prefixes
    "TextDir"   : "./text"    ,
    "ModelDir"  : "./model"   ,
    "ResultDir" : "./results" ,
    "PickleDir" : "./pickles" ,


    # Debug and diagnostics
    "EnableDiagnostics" : True ,

    "Experiments" :
    [
        {
            "WE" :
            {
                # WE training parameters
                "W2V_Size"     : 300 ,
                "W2V_Window"   : 10  ,
                "W2V_MinCount" : 5   ,
                "W2V_Workers"  : 4   ,

                # Supported algorithms:
                # - CBOW
                # - SG
                "EmbeddingAlgorithm" : "CBOW" ,
            },

            # Supported datasets:
            # - WS353
            # - MEN
            # - SimLex-999
            "UsingDataset"       : "WS353" ,

            "Sizes" :
            {
                   1000000 : "1M"   ,
                   5000000 : "5M"   ,
                  #10000000 : "10M"  ,
                  #50000000 : "50M"  ,
                 #100000000 : "100M" ,
                 #500000000 : "500M" ,
                #1000000000 : "1B"   ,
            },
        },
   ]
}

