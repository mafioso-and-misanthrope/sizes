#!/usr/bin/env python

import os.path

from sizes.configurator import *

from sizes.utils.diagnostics import measure_time

from gensim.models import Word2Vec
from gensim.models import KeyedVectors

from web.embedding import Embedding


def get_w2v_sg_parameter(algorithm_name):
    if algorithm_name == "CBOW":
        return 0
    elif algorithm_name == "SG":
        return 1
    else:
        raise Exception("Algorithm {} not supported".format(algorithm_name))


def get_model(tokens, size_label, we_settings):
    return measure_time("Prepare model " + size_label,
        __processing_model, tokens, size_label, we_settings
    )


def __processing_model(tokens, size_label, we_settings):
    filename = (settings["ModelDir"]                + "/wiki_"
                + size_label                        + "_"
                + str(we_settings["W2V_Size"])      + "_"
                + str(we_settings["W2V_Window"])    + "_"
                + we_settings["EmbeddingAlgorithm"] + ".model")

    if settings["AllowReadModel"] and os.path.isfile(filename):
        return Embedding.from_word2vec(filename)

    model = Word2Vec(tokens
                   , size      = we_settings["W2V_Size"]
                   , window    = we_settings["W2V_Window"]
                   , min_count = we_settings["W2V_MinCount"]
                   , workers   = we_settings["W2V_Workers"]
                   , sg        = get_w2v_sg_parameter(we_settings["EmbeddingAlgorithm"]))

    model.wv.save_word2vec_format(filename)
    return Embedding.from_word2vec(filename)

