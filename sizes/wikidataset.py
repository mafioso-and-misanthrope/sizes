#!/usr/bin/env python


import random
import os.path
import ast
import pickle

from sizes.configurator import *

from sizes.utils.diagnostics import measure_time
from nltk.tokenize import word_tokenize
from nltk.tokenize import sent_tokenize


def to_tokenized_sentences(corpus_iterator
                         , words_count
                         , use_random         = settings["UseRandom"]
                         , read_pickle_cache  = settings["AllowReadPickleCache"]
                         , write_pickle_cache = settings["AllowWritePickleCache"]):
    result = measure_time("Processing " + str(words_count) + " words"
                        , __corpus_processing
                        , corpus_iterator
                        , words_count
                        , use_random
                        , read_pickle_cache
                        , write_pickle_cache)
    print("Count of sentences: " + str(len(result)))
    return result


def __corpus_processing(corpus_iterator
                      , words_count_limit
                      , use_random
                      , read_pickle_cache
                      , write_pickle_cache):
    filename = settings["PickleDir"] + "/sents_" + str(words_count_limit) + ".pickle"
    if read_pickle_cache and os.path.isfile(filename):
        result = []
        with open(filename, "rb") as file:
            try:
                while True:
                    line = pickle.load(file)
                    if len(line) != 0:
                        result.extend(line)
            except EOFError:
                pass
        return result
    else:
        if write_pickle_cache:
            with open(filename, "wb+") as file:
                result_words_count, result = __get_sentences_of_articles(corpus_iterator, words_count_limit, use_random, file)
        else:
            result_words_count, result = __get_sentences_of_articles(corpus_iterator, words_count_limit, use_random, "")

        if result_words_count != words_count_limit:
            print("WARNING! Resulting count of words {} does not match with required: {}".format(result_words_count, words_count_limit))
        return result


def __get_paragraph(sentences, paragraph_words_count_limit):
    paragraph = []
    words_count = 0
    for sentence in sentences:
        tokenized_sentence = [word for word in word_tokenize(sentence) if word.isalpha()]
        paragraph.append(tokenized_sentence)
        words_count += len(tokenized_sentence)
        if words_count >= paragraph_words_count_limit:
            return paragraph_words_count_limit, paragraph[:paragraph_words_count_limit]
    return words_count, paragraph


def __get_article(marked_article, article_words_count_limit, use_random):
    article = []
    words_count = 0
    for text in marked_article["section_texts"]:
        paragraph_words_count_limit = article_words_count_limit - words_count
        if paragraph_words_count_limit <= 0:
            break
        sentences = sent_tokenize(text)
        if use_random and len(sentences) > 0:
            sentences = random.sample(sentences, random.randint(0, len(sentences) - 1))
        words_count_in_paragraph, paragraph = __get_paragraph(sentences, paragraph_words_count_limit)
        article += paragraph
        words_count += words_count_in_paragraph
    return words_count, article


def __get_sentences_of_articles(corpus_iterator, words_count_limit, use_random, file):
    words_count = 0
    result = []
    try:
        while True:
            current = next(corpus_iterator)
            if use_random:
                while random.randint(0, 100) % 7 != 0:
                    current = next(corpus_iterator)

            article_words_count_limit = words_count_limit - words_count
            if article_words_count_limit <= 0:
                break
            words_count_in_article, article = __get_article(current, article_words_count_limit, use_random)
            result += article

            if file:
                pickle.dump(article, file)

            words_count += words_count_in_article
    except StopIteration:
        pass
    return words_count, result

