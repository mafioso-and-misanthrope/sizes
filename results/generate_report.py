#!/usr/bin/env python

import pandas as pd
import matplotlib.pyplot as plt

from matplotlib.backends.backend_pdf import PdfPages

with PdfPages("report.pdf") as pp:
    df = pd.read_pickle("df_results.pickle")
    df.to_csv("results.csv")

    for i, size in enumerate(df["Size"]):
        df["Size"][i] = size // 1000000

    df.set_index(["Algorithm","Dataset"], inplace=True)
    indexes = set(df.index)

    x = "Size"
    y = ["Score for Low"
       , "Score for Middle"
       , "Score for High"
       , "Score for Mixed"
       , "General score"]

    for index in indexes:
        fig, axes = plt.subplots(ncols=2, figsize=(14,6))
        df_i = df.loc[index]
        plot_bar = df_i.plot(
            x = x,
            y = y,
            kind = "bar",
            ax=axes[0])
        plot_line = df_i.plot(
            x = x,
            y = y,
            kind = "line",
            ax=axes[1])

        for ax in axes.flat:
            ax.set(xlabel="Size, mln. words")
        axes.flat[0].set(ylabel="Spearman coefficient")

        title = "Algorithm: {}\nDataset: {}".format(index[0], index[1])
        plot_line.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
        plot_line.set_title(title)
        plot_bar.get_legend().remove()
        plot_bar.set_title(title)
        plt.xticks(df_i["Size"])
        plt.tight_layout()
        pp.savefig(bbox_inches="tight")

    df_counts = df.groupby(["Dataset"]).mean()
    plot_counts = df_counts.reset_index().plot(
        x = "Dataset",
        y = ["Count of Low"
           , "Count of Middle"
           , "Count of High"
           , "Count of Mixed"
        ],
        kind = "barh"
    )
    plt.ylabel("")
    plt.title("Counts of words pairs for each dataset")
    pp.savefig(bbox_inches="tight")

