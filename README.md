# Sizes

**Sizes** is an application for investigation of the dependency between corpus size and accuracy of resulting word-embedding model.


## Introduction and Goals
Basic idea is to evaluate the accuracy of word embedding models trained on different text sizes,
to see how the combination of corpus size and word frequency affects the results. 

Text for model training was taken from English Wikipedia (can be obtained via GensimAPI: https://github.com/RaRe-Technologies/gensim-data → wiki-english-20171001)

In this work the following approach was used:

* Words from the dataset vocabulary was divided into beans depending on how often they appear in the text.
* Each pair of words from dataset were divided into another four categories according to frequency and bins of its parts.
For example, if both words from the pair belong to high frequency bin this pair was marked as BOTH_HIGH.
If words from the pair belong to different bins pair was marked as MIXED.
* Spearman coefficient score between model and dataset similarity scores was calculated for each category.


## Requirements
This application requires enviroment with installed Python 3.5 (or higher) and pip or another package manager.

Packages to be installed:
* nltk
* gensim
* pandas
* tqdm
* matplotlib
* word-embedding benchmarks from https://github.com/kudkudak/word-embeddings-benchmarks


## Usage
To run follow next steps:
1. Clone this repo and go to repo's directory.
2. If you are using Conda, install and activate a new Conda environment:
``` bash
$ conda env install -f environment.yml
$ conda activate Sizes
``` 
If you have no Conda install all Python packages from [environment.yml](environment.yml) with pip or another package manager.

3. Configure application by editing [sizes/configurator.py](sizes/configurator.py).
4. Run [main.py](main.py). Results will be saved into ```results/df_results.pickle``` by default.
5. Execute [results/generate_report.py](results/generate_report.py) to get info from ```results/df_results.pickle``` and save results into the readable report file.
You can see the report example [here](results/report.pdf).


## Results
See [results/results.csv](results/results.csv) and [results/report.pdf](results/report.pdf)

Example for dataset SimLex-999:

![oops!](results/pictures/example_simlex_sg.png)


Example for dataset WS353:

![oops!](results/pictures/example_ws_sg.png)


## Main Findings
In almost all experiments accuracy of model was increasing with enlarging the corpus size.
Resulting score for different frequency categories strongly depends on dataset.
For example, in WS353 dataset the middle bin of the words pairs has the best score but in SimLex-999 dataset the mixed bin of the words pairs has the best score.
