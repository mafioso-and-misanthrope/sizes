#!/usr/bin/env python


import numpy as np
import pandas as pd
import time
import pickle
import os
import datetime


from sizes.configurator import *
from sizes.utils.wiki import download
from sizes.utils.wiki import get_dataset
from sizes.utils.misc import create_vocabulary
from sizes.utils.misc import on_start
from sizes.wikidataset import to_tokenized_sentences
from sizes.we_model import get_model
from sizes.extra import logo


from web.datasets.similarity import fetch_WS353
from web.datasets.similarity import fetch_MEN
from web.datasets.similarity import fetch_SimLex999
from web.evaluate import evaluate_similarity


from collections import Counter
from operator import itemgetter


def divide_into_bins_by_single_word(tokenized_sentences, vocab):
    c = Counter(word for sentence in tokenized_sentences for word in set(sentence))
    word_freq = [(word, c[word]) for word in vocab]
    word_freq.sort(key=itemgetter(1, 0))
    frequencies = [freq for (word, freq) in word_freq]
    max_freq = max(frequencies)
    word_freq_normalized = [(word, round(freq / max_freq, 5)) for (word, freq) in word_freq]
    words = [word for (word, freq) in word_freq]
    dataset_length = len(word_freq_normalized)
    part_length = dataset_length // 3
    high = words[2 * part_length:]
    high_bounds = (c[high[0]], c[high[-1]])
    mid = words[part_length:2 * part_length]
    mid_bounds = (c[mid[0]], c[mid[-1]])
    low = words[:part_length]
    low_bounds = (c[low[0]], c[low[-1]])
    return high, mid, low, high_bounds, mid_bounds, low_bounds


def divide_into_bins_by_pair_of_words(low, mid, high, dataset):
    pair_similarity ={}
    low_pair = []
    mid_pair = []
    high_pair = []
    mixed_pair = []

    low_similarity = []
    mid_similarity = []
    high_similarity = []
    mixed_similarity = []
    for pair, similarity in zip(dataset.X, dataset.y):
        if pair[0] in low and pair[1] in low:
            low_pair.append(pair)
            low_similarity.append(similarity)
            continue
        if pair[0] in mid and pair[1] in mid:
            mid_pair.append(pair)
            mid_similarity.append(similarity)
            continue
        if pair[0] in high and pair[1] in high:
            high_pair.append(pair)
            high_similarity.append(similarity)
            continue
        mixed_pair.append(pair)
        mixed_similarity.append(similarity)
    pair_similarity['low'] = (low_pair, low_similarity)
    pair_similarity['mid'] = (mid_pair, mid_similarity)
    pair_similarity['high'] = (high_pair, high_similarity)
    pair_similarity['mixed'] = (mixed_pair, mixed_similarity)
    return pair_similarity


def fetch_dataset(dataset_name):
    if dataset_name == "WS353":
        return fetch_WS353("similarity")
    elif dataset_name == "MEN":
        return fetch_MEN("all")
    elif dataset_name == "SimLex-999":
        return fetch_SimLex999()
    else:
        raise Exception("{}: dataset not supported".format(dataset_name))


def do_experiment(corpus_iterator
                , we_settings
                , dataset
                , dataset_title
                , vocab
                , sentences
                , words_count
                , size
                , size_label):
    sentences += to_tokenized_sentences(corpus_iterator, words_count=words_count)

    model = get_model(sentences, size_label, we_settings)

    high, mid, low, high_bounds, mid_bounds, low_bounds = divide_into_bins_by_single_word(sentences, vocab)

    pair_similarity = divide_into_bins_by_pair_of_words(low, mid, high, dataset)

    general_similarity = evaluate_similarity(model, dataset.X, dataset.y)
    low_bin_similarity = evaluate_similarity(model
                                           , np.asarray(pair_similarity['low'][0])
                                           , np.asarray(pair_similarity['low'][1]))
    mid_bin_similarity = evaluate_similarity(model
                                           , np.asarray(pair_similarity['mid'][0])
                                           , np.asarray(pair_similarity['mid'][1]))
    high_bin_similarity = evaluate_similarity(model
                                           , np.asarray(pair_similarity['high'][0])
                                           , np.asarray(pair_similarity['high'][1]))
    mixed_bin_similarity = evaluate_similarity(model
                                           , np.asarray(pair_similarity['mixed'][0])
                                           , np.asarray(pair_similarity['mixed'][1]))

    pickle_results(general_similarity
                 , low_bin_similarity
                 , len(pair_similarity['low'][0])
                 , mid_bin_similarity
                 , len(pair_similarity['mid'][0])
                 , high_bin_similarity
                 , len(pair_similarity['high'][0])
                 , mixed_bin_similarity
                 , len(pair_similarity['mixed'][0])
                 , low_bounds
                 , mid_bounds
                 , high_bounds
                 , we_settings
                 , dataset_title
                 , size)


def pickle_results(general_similarity
                 , low_bin_similarity
                 , low_pair_count
                 , mid_bin_similarity
                 , mid_pair_count
                 , high_bin_similarity
                 , high_pair_count
                 , mixed_bin_similarity
                 , mixed_pair_count
                 , low_bounds
                 , mid_bounds
                 , high_bounds
                 , we_settings
                 , dataset_title
                 , size):
    results_filename = settings["ResultDir"] + '/df_results.pickle'

    if os.path.isfile(results_filename):
        df = pd.read_pickle(results_filename)
    else:
        df = pd.DataFrame()

    time = datetime.datetime.now()

    df = df.append(pd.DataFrame({
        "Algorithm"        : we_settings["EmbeddingAlgorithm"] ,
        "Date"             : datetime.datetime.now()           ,
        "Window"           : we_settings["W2V_Window"]         ,
        "Size"             : size                              ,
        "Dataset"          : dataset_title                     ,
        "Low lower bound"  : low_bounds[0]                     ,
        "Low upper bound"  : low_bounds[1]                     ,
        "Mid lower bound"  : mid_bounds[0]                     ,
        "Mid upper bound"  : mid_bounds[1]                     ,
        "High lower bound" : high_bounds[0]                    ,
        "High upper bound" : high_bounds[1]                    ,
        "Score for Low"    : low_bin_similarity                ,
        "Count of Low"     : low_pair_count                    ,
        "Score for Middle" : mid_bin_similarity                ,
        "Count of Middle"  : mid_pair_count                    ,
        "Score for High"   : high_bin_similarity               ,
        "Count of High"    : high_pair_count                   ,
        "Score for Mixed"  : mixed_bin_similarity              ,
        "Count of Mixed"   : mixed_pair_count                  ,
        "General score"    : general_similarity                ,
    }, index=[0]), ignore_index=True)

    df.to_pickle(results_filename)
    print(df)


if __name__ == "__main__":
    logo()
    on_start()

    if settings["SeparatedDownload"]:
        download()

    for experiment in settings["Experiments"]:
        corpus = get_dataset()
        corpus_iterator = iter(corpus)
        dataset = fetch_dataset(experiment["UsingDataset"])
        vocab = create_vocabulary(dataset)

        sentences = []
 
        words_in_use = 0
        for size, size_label in experiment["Sizes"].items():
            words_left_to_get = size - words_in_use
            do_experiment(corpus_iterator
                        , experiment["WE"]
                        , dataset
                        , experiment["UsingDataset"]
                        , vocab
                        , sentences
                        , words_left_to_get
                        , size
                        , size_label)
            words_in_use += words_left_to_get

